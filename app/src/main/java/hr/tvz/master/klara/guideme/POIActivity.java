package hr.tvz.master.klara.guideme;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.snapshot.LocationResult;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import hr.tvz.master.klara.guideme.cardView.MyRecyclerViewAdapter;
import hr.tvz.master.klara.guideme.context.UserContext;
import hr.tvz.master.klara.guideme.geofence.Constants;
import hr.tvz.master.klara.guideme.geofence.GeofenceTransitionsIntentService;
import hr.tvz.master.klara.guideme.poi.POIGenerator;
import hr.tvz.master.klara.guideme.poi.PointOfInterest;

public class POIActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<Status>, OnMapReadyCallback {

    final private static String TAG = POIActivity.class.getSimpleName();

    private static final int MY_PERMISSION_LOCATION = 1;

    final static int POIS_TO_SHOW_IN_RADIUS_IN_METERS = 1000;

    private GoogleMap mMap;

//    double[] locationLatLng;
    double[] locationLatLng2= new double[2];
    ArrayList<PointOfInterest> pointOfInterestList = new ArrayList<>();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private SupportMapFragment mapFragment;

    protected ArrayList<Geofence> mGeofenceList=new ArrayList<>();
    protected GoogleApiClient m1GoogleApiClient;
    private PendingIntent mGeofencePendingIntent;
    protected GeofencingClient mGeofencingClient;

    private UserContext context = new UserContext();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi);
        setTitle("POI Activity");

/*        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            locationLatLng = (double[]) bundle.getSerializable("location");
        }*/
        //mAddGeofencesButton = (Button) findViewById(R.id.add_geofences_button);
        // Empty list for storing geofences.
      //  mGeofenceList = new ArrayList<Geofence>();

        // Get the geofences used. Geofence data is hard coded in this sample.
       // populateGeofenceList();

        mGeofencingClient = LocationServices.getGeofencingClient(POIActivity.this);

        // Kick off the request to build GoogleApiClient.
        this.m1GoogleApiClient = new GoogleApiClient.Builder(POIActivity.this)
                .addApi(Awareness.API)
                .addApi(LocationServices.API)
                .build();
        this.m1GoogleApiClient.connect();

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getView().setVisibility(View.GONE);
        getCurrentLocation();
        getWeather();
//        populatePointOfInterestList();
//        showCardsWithPointsOfInterest();
//        showPointsOfInterestOnTheMap();
        LocalBroadcastManager lbc = LocalBroadcastManager.getInstance(this);

        GoogleFencesReceiver receiver = new GoogleFencesReceiver();
        lbc.registerReceiver(receiver, new IntentFilter("googlegeofence"));

        BeenHereReceiver beenHereReceiver = new BeenHereReceiver();
        lbc.registerReceiver(beenHereReceiver, new IntentFilter("beenhere"));
    }

    protected void onStop() {
        m1GoogleApiClient.disconnect();
        super.onStop();
    }

    private void getWeather() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(POIActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_LOCATION);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        try {
            Awareness.SnapshotApi.getWeather(m1GoogleApiClient).setResultCallback(new ResultCallback<WeatherResult>() {
                @Override
                public void onResult(@NonNull WeatherResult weatherResult) {
                    if (!weatherResult.getStatus().isSuccess()) {
                        Log.i(TAG, "WeatherResult toString() ======> " + weatherResult.getStatus());
                        Log.i(TAG, "WeatherResult getWeather() ======> " + weatherResult.getWeather());
                        Log.i(TAG, "Could not get weather.");
                        Toast.makeText(POIActivity.this, "Could not get weather.", Toast.LENGTH_SHORT).show();
                        return;
                    }else{
                        Weather weather = weatherResult.getWeather();
                        context.setWeather(weather);
                        Log.i(TAG, "Weather: " + weather);
                        populatePointOfInterestList();
                    }
                }
            });
        }catch (Exception e){
            e.getMessage().toString();
        }
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(POIActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_LOCATION);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        Awareness.SnapshotApi.getLocation(m1GoogleApiClient).setResultCallback(new ResultCallback<LocationResult>() {
            @Override
            public void onResult(@NonNull LocationResult locationResult) {
                if (!locationResult.getStatus().isSuccess()) {
                    Log.e(TAG, "Could not get location.");
                    Toast.makeText(POIActivity.this, "Could not get location.", Toast.LENGTH_SHORT).show();
                    return;
                }
                Location location = locationResult.getLocation();
                context.setLocation(location);
                Log.i(TAG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>Lat: " + location.getLatitude() +
                        "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>Lng: " + location.getLongitude());
                locationLatLng2[0] = location.getLatitude();
                locationLatLng2[1] = location.getLongitude();
                Log.i(TAG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>locationLatLng2[0]: " + Double.toString(locationLatLng2[0]) + "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>locationLatLng2[1]: " + Double.toString(locationLatLng2[1]));
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        //This is to get at least one marker from current location.
        //That is different colour and
        //So people know where they are precisely
        LatLng currentLocation = new LatLng(locationLatLng2[0], locationLatLng2[1]);
        mMap.addMarker(new MarkerOptions()
                .position(currentLocation)
                .title("You are here")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        builder.include(currentLocation);

        //Next markers are built for every PoI
        for (int i=0; i<pointOfInterestList.size(); i++) {
            LatLng latLng = new LatLng(pointOfInterestList.get(i).getLat(), pointOfInterestList.get(i).getLng());
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(pointOfInterestList.get(i).getName()));
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20); // offset from edges of the map

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        googleMap.moveCamera(cameraUpdate);
        mapFragment.getView().setVisibility(View.VISIBLE);
    }

    public void populatePointOfInterestList() {

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //listOfPlaceTypesPerContext:
        //context means weather is day and good weather or night and bad weather, etc.
        //I am getting a list of place types when context is detected()and I have to
        final List<String> listOfPlaceTypesPerContext = POIGenerator.getPointsOfInterests(context);
/*        String url= "https://maps.googleapis.com/maps/api/place/textsearch/json?" +
                "query=Touristic+attraction+" + cityName +*/
        for (int i = 0; i < listOfPlaceTypesPerContext.size(); i++) {//i have to close for down!!!
            final int passedValue = i;
            final String typeItem = listOfPlaceTypesPerContext.get(i);
            Log.i(TAG, "PLACE_TYPE ======> " + typeItem);

            String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                    "&type=" + typeItem +
                    "&location=" + context.getLocation().getLatitude() + "," + context.getLocation().getLongitude() +
                    "&radius=" + POIS_TO_SHOW_IN_RADIUS_IN_METERS +
                    "&key=" + getString(R.string.MY_API_KEY);
            Log.i("url", url);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //System.out.println(response.toString());
                            Log.i(TAG, "Json response toString()======> " + response.toString());
                            try {
                                Log.i(TAG, "Json response getString() name:status ======> " + response.getString("status"));
                                //Toast.makeText(POIActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                                if (response.getString("status").equals("OK")) {
                                    JSONArray results = response.getJSONArray("results");
                                    for (int i = 0; i < results.length(); i++) {
                                        JSONObject object = results.getJSONObject(i);
                                        JSONObject location = object.getJSONObject("geometry").getJSONObject("location");
                                        Double lat = Double.parseDouble(location.getString("lat"));
                                        Double lng = Double.parseDouble(location.getString("lng"));
                                        Double rating = null;
                                        if (object.has("rating")) {
                                            Log.i(TAG, "JSON OBJECT - RATING ======> " + object.getString("rating"));
                                            rating = Double.parseDouble(object.getString("rating"));
                                        }
                                        String name = object.getString("name");
                                        String id = object.getString("id");
                                        Boolean isOpen = null;
                                        if (object.has("opening_hours") && object.getJSONObject("opening_hours").has("open_now")) {
                                            Log.i(TAG, "JSON OBJECT - OPENING ======> " + object.getJSONObject("opening_hours").getBoolean("open_now"));
                                            isOpen = object.getJSONObject("opening_hours").getBoolean("open_now");
                                        }

                                        PointOfInterest pointOfInterest = new PointOfInterest();
                                        pointOfInterest.setName(name);
                                        pointOfInterest.setLat(lat);
                                        pointOfInterest.setLng(lng);
                                        pointOfInterest.setId(id);
                                        pointOfInterest.setOpen(isOpen);
                                        pointOfInterest.setRating(rating);

                                        pointOfInterestList.add(pointOfInterest);
                                        Log.i(TAG, "==================================================================");
                                        Log.i(TAG, "POI type ======> " + typeItem);
                                        Log.i(TAG, (i + 1) + ". POI name: " + pointOfInterestList.get(i).getName());
                                        Log.i(TAG, "Rating: " + pointOfInterestList.get(i).getRating());
                                        Log.i(TAG, "Opening hours - Is open?: " + pointOfInterestList.get(i).isOpen());
                                        Log.i(TAG, "==================================================================");
                                    }
                                }
                            } catch (JSONException e) {
                                Log.d(TAG, "JSONException e toString() ======> " + e.toString());
                            } finally {
                                if ((passedValue + 1) == listOfPlaceTypesPerContext.size()) {
//                                        Choose five places randomly
//                                        Collections.shuffle(pointOfInterests, new Random());
//                                        if (pointOfInterests.size() > 5) {
//                                            pointOfInterests = new ArrayList<>(pointOfInterests.subList(0,5));
//                                        }

                                    // Show 'See on map' button after getting nearby places

                                    if (pointOfInterestList.isEmpty() && mGeofenceList.isEmpty()) {
                                        Toast.makeText(POIActivity.this, "Point of interest list is empty!", Toast.LENGTH_SHORT).show();
                                        Log.i(TAG, "Point of interest list is empty!");
                                    } else {
                                        showCardsWithPointsOfInterest();
                                        showPointsOfInterestOnTheMap();
                                        populateGeofenceList();
                                        addGeofences();
                                    }
                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(POIActivity.this, "An error has ocurred. Please, check your internet connection.", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "Json response error toString()======> " + error.toString());
                        }
                    });
            requestQueue.add(jsonObjectRequest);
        }
    }




    public void showCardsWithPointsOfInterest(){
//        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
//        mRecyclerView.setHasFixedSize(true);
//
//        mLayoutManager = new LinearLayoutManager(this);
//        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new MyRecyclerViewAdapter(pointOfInterestList);
        mRecyclerView.setAdapter(mAdapter);

        ((MyRecyclerViewAdapter) mAdapter).setOnItemClickListener(new MyRecyclerViewAdapter
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Log.i(TAG, " Clicked on Item ======> " + position);
                PointOfInterest poi = pointOfInterestList.get(position);
                if (!poi.isBeenHere() && poi.isInRadius()) {
                    Intent intent = new Intent(POIActivity.this, POIDetailsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("POI", poi);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }

    public void showPointsOfInterestOnTheMap() {
        mapFragment.getMapAsync(POIActivity.this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        m1GoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Status status) {

    }

    //final AsyncTask<Params, Progress, Result>
    private class showPointsOfInterestOnMapAndInCardsTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }

    /*
    *
    * GEOFENCES
    *
     */

    private void populateGeofenceList() {
        for (int i = 0; i < pointOfInterestList.size(); i++) {
            PointOfInterest pointOfInterest = pointOfInterestList.get(i);
            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(Integer.toString(i))

                    .setCircularRegion(
                            pointOfInterest.getLat(),
                            pointOfInterest.getLng(),
                            Constants.GEOFENCE_RADIUS_IN_METERS
                    )
                    .setExpirationDuration(Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build()
            );
        }
    }


    public void addGeofences() {
        if (!m1GoogleApiClient.isConnected()) {
            Toast.makeText(POIActivity.this, "Google API Client not connected!", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            // Geofences added
                            // ...
                            Toast.makeText(POIActivity.this, "Geofences added!", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Failed to add geofences
                            // ...
                            Toast.makeText(POIActivity.this, "Geofences NOT added!", Toast.LENGTH_SHORT).show();
                        }
                    });
            /*LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    getGeofencingRequest(),
                    getGeofencePendingIntent()
            ).setResultCallback(this); // Result processed in onResult().*/
        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            securityException.getMessage();
        }
    }
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.

        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(POIActivity.this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(POIActivity.this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    class GoogleFencesReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Log.i(TAG,"GoogleFencesReceiver: Received bundle ======> " + bundle);
            for (String id: bundle.keySet()){
                Log.i(TAG,"GoogleFencesReceiver: Received bundle keySet()======> " + bundle.keySet());
                Integer identifier = Integer.parseInt(id);
                PointOfInterest poi = pointOfInterestList.get(identifier);
                if (bundle.get(id).toString().equals("ENTER")) {
                    poi.setInRadius(true);
                } else {
                    poi.setInRadius(false);
                }
            }
            mAdapter.notifyDataSetChanged();
        }
    }


    class BeenHereReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            Bundle bundle = intent.getExtras();
            Log.i(TAG,"BeenHereReceiver: Received bundle ======> " + bundle);

            // Write a message to the database
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference();

            for (String id: bundle.keySet()){
                Log.i(TAG,"BeenHereReceiver: Received bundle keySet()======> " + bundle.keySet());
                //Integer identifier = Integer.parseInt(id);

                PointOfInterest poi = null;
                for(PointOfInterest item : pointOfInterestList) {
                    if (id.equals(item.getId())) {
                        poi = item;
                        break;
                    }
                }
                boolean checkBoxBeenHereValue = bundle.getBoolean(id);
                poi.setBeenHere(checkBoxBeenHereValue);
                if (poi.isBeenHere()) {
                    myRef.child("pois").child(mAuth.getCurrentUser().getUid()).child(poi.getId()).setValue(poi);
                }else{
                    myRef.child("pois").child(mAuth.getCurrentUser().getUid()).child(poi.getId()).removeValue();
                }
            }
            mAdapter.notifyDataSetChanged();
        }
    }


}


