package hr.tvz.master.klara.guideme;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.snapshot.DetectedActivityResult;
import com.google.android.gms.awareness.snapshot.LocationResult;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;


public class CurrentContextActivity extends AppCompatActivity{

    private static final String TAG = CurrentContextActivity.class.getSimpleName();
    private static final int MY_PERMISSION_LOCATION = 1 ;

    private TextView textView_snapshotData;
    private GoogleApiClient mGoogleApiClient;

    private Button buttonPOI;
    private Location location;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_context);

        textView_snapshotData = findViewById(R.id.googleSnapshotApi_textView);

        this.mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(Awareness.API).build();
        this.mGoogleApiClient.connect();

        showSnapshotData();

        buttonPOI = findViewById(R.id.button_poi);
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }
    public void onClickShowPOIActivity(View view) {

        Bundle bundle = new Bundle();
        Intent intent = new Intent(CurrentContextActivity.this, POIActivity.class);
        try {
            double[] locationLatLng = new double[]{location.getLatitude(), location.getLongitude()};
            bundle.putSerializable("location", locationLatLng);
            intent.putExtras(bundle);
            startActivity(intent);
        }catch (Exception e){
            e.getMessage();
        }
        
    }

    private void showSnapshotData() {
        textView_snapshotData.setText("");
        getWeather();
        getLocation();
        getCurrentActivity();
    }

    private void getWeather() {
        checkFineLocationPermission();
        try {
            Awareness.SnapshotApi.getWeather(mGoogleApiClient).setResultCallback(new ResultCallback<WeatherResult>() {
                @Override
                public void onResult(@NonNull WeatherResult weatherResult) {
                    if (!weatherResult.getStatus().isSuccess()) {
                        Log.i(TAG, "WeatherResult toString() ======> " + weatherResult.getStatus());
                        Log.i(TAG, "WeatherResult getWeather() ======> " + weatherResult.getWeather());
                        Log.i(TAG, "Could not get weather.");
                        Toast.makeText(CurrentContextActivity.this, "Could not get weather.", Toast.LENGTH_SHORT).show();
                        return;
                    }else{
                        Weather weather = weatherResult.getWeather();
                        Log.i(TAG, "Weather: " + weather);
                        textView_snapshotData.append("\n___\nWeather: " + weather);
                    }
                }
            });
        }catch (Exception e){
            e.getMessage().toString();
        }
    }
    private void checkFineLocationPermission() {
        if (ContextCompat.checkSelfPermission(CurrentContextActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(CurrentContextActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_LOCATION);
        }
    }

    private void getLocation() {
        checkFineLocationPermission();
        try {
            Awareness.SnapshotApi.getLocation(mGoogleApiClient).setResultCallback(new ResultCallback<LocationResult>() {
                @Override
                public void onResult(@NonNull LocationResult locationResult) {
                    if (!locationResult.getStatus().isSuccess()) {
                        Log.i(TAG, "LocationResult toString() ======> " + locationResult.getStatus());
                        Log.i(TAG, "LocationResult getLocation() ======> " + locationResult.getLocation());
                        Log.i(TAG, "Could not get location.");
                        Toast.makeText(CurrentContextActivity.this, "Could not get place likelihoods.", Toast.LENGTH_SHORT).show();
                        return;
                    }else{
                        location = locationResult.getLocation();
                        Log.i(TAG, "Lat: " + location.getLatitude() + ", Lng: " + location.getLongitude());
                        textView_snapshotData.append("\n___\nLocation: " + "Lat: " + location.getLatitude() + ", Lng: " + location.getLongitude());
                    }
                }
            });
        }catch (Exception e){
            e.getMessage().toString();
        }

    }

    private void getCurrentActivity() {
        try{
            Awareness.SnapshotApi.getDetectedActivity(mGoogleApiClient).setResultCallback(new ResultCallback<DetectedActivityResult>() {
                @Override
                public void onResult(@NonNull DetectedActivityResult detectedActivityResult) {
                    if (!detectedActivityResult.getStatus().isSuccess()) {
                        Log.e(TAG, "Could not get the current activity.");
                        Toast.makeText(CurrentContextActivity.this, "Could not get the current activity.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    ActivityRecognitionResult ar = detectedActivityResult.getActivityRecognitionResult();
                    DetectedActivity mostProbableActivity = ar.getMostProbableActivity();
                    Log.i(TAG, mostProbableActivity.toString());
                    textView_snapshotData.append("\n___\nCurrent Activity: " + mostProbableActivity.toString());
                }
            });
        }catch (Exception e){
            e.getMessage().toString();
        }
    }


}