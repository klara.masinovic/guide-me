package hr.tvz.master.klara.guideme;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import hr.tvz.master.klara.guideme.poi.PointOfInterest;

public class POIDetailsActivity extends AppCompatActivity {

    private static final String TAG = POIDetailsActivity.class.getSimpleName();

    private CheckBox checkBoxBeenHere;
    private TextView textViewDetailsFullText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poidetails);

        textViewDetailsFullText = findViewById(R.id.detailsFullText_textView);
        checkBoxBeenHere = findViewById(R.id.beenHere_checkBox);

        Intent intent = getIntent();
        final PointOfInterest poi = (PointOfInterest) intent.getSerializableExtra("POI");
        setTitle(poi.getName());
        checkBoxBeenHere.setChecked(poi.isBeenHere());

        textViewDetailsFullText.append(poi.getLat() +
                "\n" +
                poi.getLng() +
                "\n" +
                (poi.getRating() != null ? poi.getRating() : "Not available") +
                "\n" +
                (poi.isOpen() != null ? poi.isOpen() : "Not available"));

        final Context self = this;

        checkBoxBeenHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lbcIntentBeenHere = new Intent("beenhere");
                // Checks if view is checked
                boolean checkBoxValue =((CheckBox) v).isChecked();
                lbcIntentBeenHere.putExtra(poi.getId(), checkBoxValue);
                LocalBroadcastManager.getInstance(self).sendBroadcast(lbcIntentBeenHere);  //Send the intent
            }
        });
    }

}
