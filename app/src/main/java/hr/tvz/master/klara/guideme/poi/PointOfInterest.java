package hr.tvz.master.klara.guideme.poi;

import java.io.Serializable;

/**
 * Created by Klara on 12/17/2017.
 */

public class PointOfInterest implements Serializable {

    private Double lat;
    private Double lng;

    private Double rating;

    private String name;
    private String id;

    private Boolean isOpen;

    private boolean beenHere;
    private boolean inRadius;

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Boolean isOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public boolean isInRadius() {
        return inRadius;
    }

    public void setInRadius(boolean inRadius) {
        this.inRadius = inRadius;
    }

    public boolean isBeenHere() {
        return beenHere;
    }

    public void setBeenHere(boolean beenHere) {
        this.beenHere = beenHere;
    }
}


