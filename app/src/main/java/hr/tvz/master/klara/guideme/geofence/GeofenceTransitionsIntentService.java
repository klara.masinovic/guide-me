package hr.tvz.master.klara.guideme.geofence;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import hr.tvz.master.klara.guideme.R;

/**
 * Created by Klara on 1/3/2018.
 */

public class GeofenceTransitionsIntentService extends IntentService {


    final private static String TAG = GeofenceTransitionsIntentService.class.getSimpleName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public GeofenceTransitionsIntentService(String name) {
        super(name);
    }

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    @SuppressLint("StringFormatInvalid")
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceErrorMessages.getErrorString(this,
                    geofencingEvent.getErrorCode());
            Log.e(TAG,"Error message ======> " + errorMessage);
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            Log.e(TAG, "Geofences detected." +
                    geofenceTransition);
            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            //List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            // Get the geofences that were triggered. A single event can trigger multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            Intent lbcIntent = new Intent("googlegeofence"); //Send to any reciever listening for this

            for(Geofence geofence : triggeringGeofences) {
                if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                    lbcIntent.putExtra(geofence.getRequestId(), "ENTER");
                } else {
                    lbcIntent.putExtra(geofence.getRequestId(), "EXIT");
                }
            }

            LocalBroadcastManager.getInstance(this).sendBroadcast(lbcIntent);  //Send the intent

        } else {
            // Log the error.
            Log.e(TAG, "Geofence transition invalid type ======> logging error ======> " + getString(R.string.geofence_transition_invalid_type,
                    geofenceTransition));
        }
    }

}