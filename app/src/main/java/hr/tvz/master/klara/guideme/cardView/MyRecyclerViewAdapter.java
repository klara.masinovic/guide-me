package hr.tvz.master.klara.guideme.cardView;

/**
 * Created by Klara on 12/19/2017.
 */

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hr.tvz.master.klara.guideme.R;
import hr.tvz.master.klara.guideme.poi.PointOfInterest;

public class MyRecyclerViewAdapter extends RecyclerView
        .Adapter<MyRecyclerViewAdapter
        .DataObjectHolder> {
    private static final String TAG = MyRecyclerViewAdapter.class.getSimpleName();
    private ArrayList<PointOfInterest> mDataSet;//DataObject je meni POI
    private static MyClickListener myClickListener;


    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {

        TextView namePOI;
        TextView latLngPOI;

        ImageView beenHereMark;
        ImageView goToDetailsArrow;

        public DataObjectHolder(View itemView) {
            super(itemView);

            namePOI = (TextView) itemView.findViewById(R.id.textView);
            latLngPOI = (TextView) itemView.findViewById(R.id.textView2);
            beenHereMark = itemView.findViewById(R.id.beenHereMark_imageView);
            goToDetailsArrow = itemView.findViewById(R.id.goToDetailsArrow_imageView);

            Log.i(TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public MyRecyclerViewAdapter(ArrayList<PointOfInterest> myDataSet) {
        mDataSet = myDataSet;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.namePOI.setText(mDataSet.get(position).getName());
        holder.latLngPOI.setText(Double.toString(mDataSet.get(position).getLat())+","+Double.toString(mDataSet.get(position).getLng()));
        holder.beenHereMark.setImageResource(R.drawable.ic_action_beenhere);
        holder.goToDetailsArrow.setImageResource(R.drawable.ic_action_gotodetails);

        PointOfInterest poi = mDataSet.get(position);
        if (poi.isInRadius() && !poi.isBeenHere()) {
            holder.namePOI.setTextColor(Color.GRAY);
            holder.latLngPOI.setTextColor(Color.GRAY);
            holder.beenHereMark.setVisibility(View.GONE);
            holder.goToDetailsArrow.setVisibility(View.VISIBLE);
        }
        else if (poi.isInRadius() && poi.isBeenHere()) {
            holder.namePOI.setTextColor(Color.GRAY);
            holder.latLngPOI.setTextColor(Color.GRAY);
            holder.beenHereMark.setVisibility(View.VISIBLE);
            holder.goToDetailsArrow.setVisibility(View.GONE);
        }
        else if (!poi.isInRadius() && !poi.isBeenHere()) {
            holder.namePOI.setTextColor(Color.LTGRAY);
            holder.latLngPOI.setTextColor(Color.LTGRAY);
            holder.beenHereMark.setVisibility(View.GONE);
            holder.goToDetailsArrow.setVisibility(View.GONE);
        }
        else {
            holder.namePOI.setTextColor(Color.LTGRAY);
            holder.latLngPOI.setTextColor(Color.LTGRAY);
            holder.beenHereMark.setVisibility(View.VISIBLE);
            holder.goToDetailsArrow.setVisibility(View.GONE);
        }
    }

/*
    public void addItem(PointOfInterest pointOfInterest, int index) {
        mDataSet.add(index, pointOfInterest);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataSet.remove(index);
        notifyItemRemoved(index);
    }
*/

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

/*    @ColorInt
    public static int getThemeColor
            (
                    @NonNull final Context context,
                    @AttrRes final int attributeColor
            )
    {
        final TypedValue value = new TypedValue();
        context.getTheme ().resolveAttribute (attributeColor, value, true);
        return value.data;
    }*/
}