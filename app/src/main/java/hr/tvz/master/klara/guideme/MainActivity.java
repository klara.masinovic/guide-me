package hr.tvz.master.klara.guideme;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import hr.tvz.master.klara.guideme.firebase.LoginActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
//    private ProgressBar bar;

    private TextView userTextView;
//    private Button loginButtonMainActivity;
    private Button currentContextButton;
    private Button startTourButton;
    private Button logOutButton;
    private Button tourHistoryButton;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        bar = (ProgressBar) findViewById(R.id.progressBar_progressTime);
//        startProgress();
        mAuth = FirebaseAuth.getInstance();



        userTextView = findViewById(R.id.mainActivity_textView_user);
//        loginButtonMainActivity = findViewById(R.id.button_logIn);
        currentContextButton = findViewById(R.id.mainActivity_button_currentContext);
        startTourButton = findViewById(R.id.mainActivity_button_startTour);
        logOutButton = findViewById(R.id.mainActivity_button_logOut);
        tourHistoryButton = findViewById(R.id.mainActivity_button_tourHistory);

        checkWhoIsLoggedIn();


        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mAuth.signOut();
//                mAuth.addAuthStateListener(mAuthListener);
                // this listener will be called when there is change in firebase user session
                mAuthListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        //that means user has logged in
                        if(firebaseAuth.getCurrentUser() == null){

//                    Intent sendLoggedUserDataToMainActivity= new Intent(LoginActivity.this,MainActivity.class);
//                    sendLoggedUserDataToMainActivity.putExtra(USER_INFO,firebaseAuth.getCurrentUser().getEmail().toString());
                            startActivity(new Intent(MainActivity.this, LoginActivity.class));
                            finish();
                        }
                    }
                };
                mAuth.addAuthStateListener(mAuthListener);
                mAuth.signOut();

            }
        });


        currentContextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCurrentContextActivity();
            }
        });
        startTourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPOIActivity();
            }
        });
        tourHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTourHistoryActivity();
            }
        });

//        loginButtonMainActivity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openLoginActivity();
//            }
//        });

  //      FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

//        Intent receivedLoggedUserData = getIntent();
//        String userInfo = receivedLoggedUserData.getStringExtra(LoginActivity.USER_INFO);

//        if(!user.getEmail().toString().isEmpty()) {
//            Log.i(TAG + "||||||USER E-MAIL||||||",user.getEmail().toString());
//            userTextView = findViewById(R.id.textView_user);
//            userTextView.setText("Hello " + user.getEmail().toString().substring(0,user.getEmail().toString().indexOf("@")) + "!");
//            userTextView.setVisibility(View.VISIBLE);
//            currentContextButton.setVisibility(View.VISIBLE);
//            startTourButton.setVisibility(View.VISIBLE);
//            loginButtonMainActivity.setVisibility(View.GONE);
//        }else {
//            userTextView.setVisibility(View.GONE);
//            currentContextButton.setVisibility(View.GONE);
//            startTourButton.setVisibility(View.GONE);
//            loginButtonMainActivity.setVisibility(View.VISIBLE);
//        }







    }

    private void checkWhoIsLoggedIn() {
        if (mAuth.getInstance().getCurrentUser() == null) {
            // Take user to log in screen
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        }else {
            Log.i(TAG,"mAuth getInstance() getCurrentUser() ======> " + mAuth.getInstance().getCurrentUser().toString());
            // User already logged in
            String userEmail = mAuth.getInstance().getCurrentUser().getEmail();
            userTextView.setText("Hello " + userEmail);
        }
    }


//    private void openLoginActivity() {
//       startActivity(new Intent(MainActivity.this, LoginActivity.class));
//    }


    private void openCurrentContextActivity() {
        startActivity(new Intent(MainActivity.this, CurrentContextActivity.class));
    }

    private void openPOIActivity(){
        startActivity(new Intent(MainActivity.this, POIActivity.class));
    }

    private void openTourHistoryActivity() {
        startActivity(new Intent(MainActivity.this, TourHistoryActivity.class));
    }

//    public void startProgress() {
//        bar.setProgress(0);
//        Log.i(TAG,"Bar.progress set to 0");
//        Log.i(TAG,"Starting thread...");
//        new Thread(new Task()).start();
//    }
//
//    class Task implements Runnable {
//        @Override
//        public void run() {
//            for (int i = 0; i <= 5; i++) {
//                final int value = i;
//                try {
//                    Log.i(TAG,"Thread sleep 1000 ms...");
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                Log.i(TAG,"bar.progress will be set to " + value);
//                bar.setProgress(value);
//
//            }
//        }
//    }





}
