package hr.tvz.master.klara.guideme;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import hr.tvz.master.klara.guideme.cardView.MyRecyclerViewAdapter;
import hr.tvz.master.klara.guideme.poi.PointOfInterest;

public class TourHistoryActivity extends AppCompatActivity {

    private static final String TAG = TourHistoryActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private FirebaseAuth mAuth;
    private DatabaseReference myRef;

    private ArrayList<PointOfInterest> userPOIList = new ArrayList<>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_history);

        mRecyclerView = (RecyclerView) findViewById(R.id.databaseOutput_recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAuth = FirebaseAuth.getInstance();

        myRef = FirebaseDatabase.getInstance().getReference();

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                for (DataSnapshot child : dataSnapshot.child("pois").child(mAuth.getCurrentUser().getUid()).getChildren()) {
                    userPOIList.add(child.getValue(PointOfInterest.class));
                }

                showCardsWithDatabaseOutput();//jer je asinkrono!
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public void showCardsWithDatabaseOutput(){
        mAdapter = new MyRecyclerViewAdapter(userPOIList);
        mRecyclerView.setAdapter(mAdapter);
    }



}
