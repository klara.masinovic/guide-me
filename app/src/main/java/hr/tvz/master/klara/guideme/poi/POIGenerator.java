package hr.tvz.master.klara.guideme.poi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import hr.tvz.master.klara.guideme.context.UserContext;

/**
 * Created by Klara on 3/14/2018.
 */

public class POIGenerator {

    public static ArrayList<String> getPointsOfInterests(UserContext context) {
        Calendar c = Calendar.getInstance();
        int hours = c.get(Calendar.HOUR_OF_DAY);
        ArrayList<String> placeTypes;
        // We are checking if it is day or night
        if (hours > 6 && hours < 20) {
            if (context.isGoodWeather()) {
                placeTypes = getTypesForDayAndGoodWeather();
            } else {
                placeTypes = getTypesForDayAndBadWeather();
            }
        } else {
            if (context.isGoodWeather()) {
                placeTypes = getTypesForNightAndGoodWeather();
            } else {
                placeTypes = getTypesForNightAndBadWeather();
            }
        }
        return placeTypes;
    }

    public static ArrayList<String> getTypesForDayAndGoodWeather() {
        return new ArrayList(Arrays.asList("park", "art_gallery", "stadium", "zoo", "museum", "university", "church", "hindu_temple", "mosque", "synagogue", "amusement_park"));
    }

    public static ArrayList<String> getTypesForDayAndBadWeather() {
        return new ArrayList(Arrays.asList("art_gallery", "restaurant", "book_store", "cafe", "library", "museum", "aquarium", "shopping_mall"));
    }

    public static ArrayList<String> getTypesForNightAndGoodWeather() {
        return new ArrayList(Arrays.asList("night_club", "bar", "movie_teather", "park", "restaurant", "meal_takeaway"));
    }

    public static ArrayList<String> getTypesForNightAndBadWeather() {
        return new ArrayList(Arrays.asList("movie_teather", "bar", "meal_delivery"));
    }
}
