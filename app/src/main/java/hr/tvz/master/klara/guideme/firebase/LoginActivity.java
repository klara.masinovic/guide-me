package hr.tvz.master.klara.guideme.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import hr.tvz.master.klara.guideme.MainActivity;
import hr.tvz.master.klara.guideme.R;

public class LoginActivity extends AppCompatActivity {

    public static final String USER_INFO = "hr.tvz.master.klara.guideme.firebase.LoginActivity.USER_INFO";
    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText eMailField;
    private EditText passwordField;
    private Button logInButton;
    private Button registerButton;
    private Button resetPasswordButton;

    private ProgressBar progressBar;

    private FirebaseAuth mAuth;

    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle(R.string.activity_login_title);

        mAuth = FirebaseAuth.getInstance();


        eMailField = findViewById(R.id.loginActivity_editText_email);
        passwordField = findViewById(R.id.loginActivity_editText_password);
        logInButton = findViewById(R.id.loginActivity_button_login);
        registerButton = findViewById(R.id.loginActivity_button_register);
        resetPasswordButton = findViewById(R.id.loginActivity_button_resetPassword);

        progressBar = findViewById(R.id.loginActivity_progressBar);
        progressBar.setVisibility(View.GONE);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //that means user has logged in
                if(firebaseAuth.getCurrentUser() != null){
//                    Intent sendLoggedUserDataToMainActivity= new Intent(LoginActivity.this,MainActivity.class);
//                    sendLoggedUserDataToMainActivity.putExtra(USER_INFO,firebaseAuth.getCurrentUser().getEmail().toString());
                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                }else{}
            }
        };



        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        resetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //resetPassword();
                //startActivity(new Intent(LoginActivity.this, ResetPasswordActivity.class));
            }
        });

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.addAuthStateListener(mAuthListener);
                logIn();
            }
        });
    }

    private void logIn() {
        String email = eMailField.getText().toString();
        String password = passwordField.getText().toString();

        //if one of the fields is empty
        //make toast
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(LoginActivity.this, R.string.empty_fields, Toast.LENGTH_LONG).show();
        }else{
            progressBar.setVisibility(View.VISIBLE);
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    progressBar.setVisibility(View.GONE);
                    //task successfull user logged inn
                    //if task is not successfull
                    //make toast
                    if (!task.isSuccessful()) {
                        Toast.makeText(LoginActivity.this, getString(R.string.login_problem), Toast.LENGTH_LONG).show();
                    } else {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }
                }
            });
        }

    }
}
