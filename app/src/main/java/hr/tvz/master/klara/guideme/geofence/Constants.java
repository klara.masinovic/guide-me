package hr.tvz.master.klara.guideme.geofence;

/**
 * Created by Klara on 1/3/2018.
 */


public class Constants {

    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = 12 * 60 * 60 * 1000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 100;

}