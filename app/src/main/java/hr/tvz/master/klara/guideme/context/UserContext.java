package hr.tvz.master.klara.guideme.context;

import android.location.Location;
import com.google.android.gms.awareness.state.Weather;

/**
 * Created by Klara on 12/17/2017.
 */

public class UserContext {
    private Weather weather;
    private Location location;

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isGoodWeather(){
        int[] weatherConditions = this.getWeather().getConditions();

//        int CONDITION_CLEAR = 1;
//        int CONDITION_CLOUDY = 2;
        for (int i=0; i<weatherConditions.length; i++) {
            if (weatherConditions[i] == 1 || weatherConditions[i] == 2) {
                return true;
            }
        }
        return false;
    }

}

